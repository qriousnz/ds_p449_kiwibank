import pandas as pd
import os, random, re
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
from collections import Counter
from collections import OrderedDict
import pickle, itertools as it
import datetime as dt
from collections import defaultdict
import plotly.offline as py
import plotly.graph_objs as go
import plotly.io as pio
import igraph
from igraph import *
from pprint import pprint

def get_duration(file):
    with open(file) as f:
            for line in f.readlines():
                if line.startswith('start.time'):
                    start_time = dt.datetime.strptime(line.split('=')[1].split('.')[0],'%d/%m/%Y %H:%M:%S')

                if line.startswith('end.time'):
                    end_time = dt.datetime.strptime(line.split('=')[1].split('.')[0],'%d/%m/%Y %H:%M:%S')
    return (end_time - start_time).total_seconds()

def searching_all_files(directory):
    dirpath = Path(directory)
    assert(dirpath.is_dir())
    file_list = []
    for x in dirpath.iterdir():
        if x.is_file():
            file_list.append(x)
        elif x.is_dir():
            file_list.extend(searching_all_files(x))
    return file_list

def show_file(file):
    with open(file) as f:
        print(f.read())
        
def is_Sublist(sublist, lst):
    if not isinstance(sublist, list):
        raise ValueError("sublist must be a list")
    if not isinstance(lst, list):
        raise ValueError("lst must be a list")

    sublist_len = len(sublist)
    list_len = len(lst)
    k=0
    s=None

    if (sublist_len > list_len):
        return False
    elif (sublist_len == 0):
        return False

    for k in range(list_len-sublist_len+1):
        if sublist == lst[k:k+sublist_len]:
            return True

    return False

#Kaplan–Meier: 
def survival_prob(path, paths_HANGUP_, paths_ALL_):
    #paths_HANGUP_ = [p for p in paths_HANGUP ]
    #paths_ALL_ = [p for p in paths_ALL]
    
    if len(path) > 1:
        path_ = path.split('_')
        path_ = [path_[:i] for i in range(1,len(path_)+1)]
    else:
        path_ = [path] 
    prob = 1
    for p_ in path_[0:]:
        prob *= 1-len([p for p in paths_HANGUP_ if p == p_])/(len([p for p in paths_ALL_ if is_Sublist(p_,p)])+0.000001)
    return prob


def mapper_service_osn(entry, ism):
    pt = entry["outbound_pt"]
    pu = entry["outbound_pu"]
    pd = entry["outbound_pd"]
    res = ism.query('Pt == @pt and Pu == @pu and Pd == @pd').to_dict(orient='records')
    if len(res) == 1:
        return  {'Queue':res[0]['Queue'], 'Tier': res[0]['Tier']}
    else:
        return  {'Queue':'none', 'Tier': 'none'}

    
def mapper_service_ssask(entry, ism):
    pt = entry["request_pt"]
    pu = entry["request_pu"]
    pd = entry["request_pd"]
    res = ism.query('Pt == @pt and Pu == @pu and Pd == @pd').to_dict(orient='records')
    if len(res) == 1:
        return  {'Queue':res[0]['Queue'], 'Tier': res[0]['Tier']}
    else:
        return  {'Queue':'none', 'Tier': 'none'}
    
# def is_Sublist(sublist, lst):
#     if not isinstance(sublist, list):
#         raise ValueError("sublist must be a list")
#     if not isinstance(lst, list):
#         raise ValueError("lst must be a list")

#     sublist_len = len(sublist)
#     k=0
#     s=None

#     if (sublist_len > len(lst)):
#         return False
#     elif (sublist_len == 0):
#         return True

#     for x in lst:
#         if x == sublist[k]:
#             if (k == 0): s = x
#             elif (x != s): s = None
#             k += 1
#             if k == sublist_len:
#                 return True
#         elif k > 0 and sublist[k-1] != s:
#             k = 0

#     return False
